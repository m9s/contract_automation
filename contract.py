#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import sys
import copy
import datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields, Model
from trytond.pyson import Eval, Not, In, Bool, Get
from trytond.transaction import Transaction
from trytond.pool import Pool


_INTERTYPES = {
    'year': lambda interval: relativedelta(years=interval),
    'quarter': lambda interval: relativedelta(months=3*interval),
    'day': lambda interval: relativedelta(days=interval),
    'hour': lambda interval: relativedelta(hours=interval),
    'week': lambda interval: relativedelta(weeks=interval),
    'month': lambda interval: relativedelta(months=interval),
    'minute': lambda interval: relativedelta(minutes=interval),
    }


class ContractActionType(ModelSQL, ModelView):
    'Contract Action Type'
    _name = 'contract.action.type'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy')
    function = fields.Char('Function', required=True, readonly=True)
    model = fields.Char('Model', required=True, readonly=True)

ContractActionType()


class ContractAction(ModelSQL, ModelView):
    'Contract Action'
    _name = 'contract.action'
    _description = __doc__
    _rec_name = 'contract'

    contract = fields.Many2One('contract.contract', 'Contract', required=True,
            ondelete='CASCADE')
    type = fields.Many2One('contract.action.type', 'Type', required=True,
            ondelete='RESTRICT')
    sequence = fields.Integer('Sequence', required=True)
    automation_interval_number = fields.Integer('Automation Interval Number',
            required=True)
    automation_interval_unit = fields.Selection([
            ('day', 'Day(s)'),
            ('week', 'Week(s)'),
            ('month', 'Month(s)'),
            ('quarter', 'Quarter(s)'),
            ('year', 'Year(s)')
            ], 'Automation Interval Unit',
            required=True)
    next_automation_date = fields.DateTime('Next Automation Date',
            select=1, readonly=True)

    def default_sequence(self):
        return 10

    def default_automation_interval_number(self):
        return 1

    def default_automation_interval_unit(self):
        return 'month'

    def process_contract_actions(self):
        action_ids = self.get_actions_to_process()
        actions = self.browse(action_ids)
        for action in actions:
            args = []
            obj = Pool().get(action.type['model'])
            if not obj and hasattr(obj, action.type['function']):
                continue
            fct = getattr(obj, action.type['function'])
            fct(action, *args)
            self.set_next_automation_date(action)
            self.terminate_contract(action)

    def get_actions_to_process(self):
        now = datetime.datetime.now()
        args = ['AND', ['OR', ('next_automation_date', '<', now),
                ['AND', ('next_automation_date', '=', False),
                ('contract.start_date', '<=', now)]],
                ('contract.automation', '=', True),
                ('contract.state', '=', 'active')]
        action_ids = self.search(args)
        return action_ids

    def set_next_automation_date(self, action):
        date_obj = Pool().get('ir.date')
        contract_obj = Pool().get('contract.contract')

        # intervals must be computed for company timezone
        company_id = action.contract.company.id
        if action.next_automation_date:
            current_date = date_obj.local2companytime(company_id,
                    action.next_automation_date)
        else:
            start_date = date_obj.local2companytime(company_id,
                    action.contract.start_date)
            current_date = self.get_first_of_interval(start_date,
                    action.automation_interval_unit)

        next_date = current_date + \
                _INTERTYPES[action.automation_interval_unit](
                       action.automation_interval_number)
        next_date = date_obj.company2localtime(company_id, next_date)

        if action.contract.end_date:
            if next_date > action.contract.end_date:
                next_date = False
        vals = {
                'next_automation_date': next_date,
                }
        self.write(action.id, vals)

    def terminate_contract(self, action):
        contract_obj = Pool().get('contract.contract')

        # terminate contracts that have reached their end date
        now = datetime.datetime.now()
        if action.contract.end_date and action.contract.end_date <= now:
            if not action.next_automation_date:
                contract_obj.workflow_trigger_validate(action.contract.id,
                        'terminate')

    def get_first_of_interval(self, date, interval):
        '''
        Return the first datetime of an automation interval for a given datetime
        and interval (with beginning always set to 00:00).
        Override this method for different special needs of the company (i.e.
        if intervals should be kept strict in relation to their start date)

        :param date: a date or datetime object
        :param interval: an interval according to automation_interval_unit
        :return: a datetime object
        '''
        date = copy.copy(date)
        if isinstance(date, datetime.datetime):
            date = date.date()
        year = date.year
        month = date.month
        day = date.day

        if interval == 'year':
            res = datetime.datetime(year, 1, 1)
        elif interval == 'quarter':
            if month in (1, 2, 3):
                month = 1
            elif month in (4, 5, 6):
                month = 4
            elif month in (7, 8, 9):
                month = 7
            elif month in (10, 11, 12):
                month = 10
            res = datetime.datetime(year, month, 1)
        elif interval == 'month':
            res = datetime.datetime(year, month, 1)
        elif interval == 'week':
            date = date - datetime.timedelta(days=date.weekday())
            res = datetime.datetime(date.year, date.month, date.day)
        elif interval == 'day':
            res = datetime.datetime(year, month, day)
        return res


    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['next_automation_date'] = False
        return super(ContractAction, self).copy(ids, default=default)

ContractAction()


class Contract(ModelWorkflow, ModelSQL, ModelView):
    _name = 'contract.contract'

    automation = fields.Boolean('Automation', select=2, states={
            'readonly': In(Eval('state'), ['cancel', 'terminated'])
            }, depends=['state'])
    actions = fields.One2Many('contract.action', 'contract', 'Actions',
            states={
                'invisible': Not(Bool(Eval('automation'))),
                'readonly': In(Eval('state'), ['cancel', 'terminated'])
                }, depends=['state', 'automation'])

    def __init__(self):
        super(Contract, self).__init__()
        self._error_messages.update({
            'action_pending': 'There are pending actions for this contract!\n\n'
                    'A contract can only be terminated after its last '
                    'scheduled action has been performed.',
            })

    def default_automation(self):
        return False

    def check_terminate(self, contract_id):
        action_obj = Pool().get('contract.action')

        res = True
        # Check if any action is scheduled to run
        action_ids = action_obj.search([
                ('contract', '=', contract_id),
                ('next_automation_date', '!=', False)
                ])
        if action_ids:
            res = False
            self.raise_user_error('action_pending')

        # When no action is scheduled, contract end_date must be (before)
        # now to *terminate* a contract
        if not action_ids:
            res = super(Contract, self).check_terminate(contract_id)
        return res


Contract()


class ContractPartyType(ModelSQL, ModelView):
    'Contract Party Type'
    _name = 'contract.party.type'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy')
    code = fields.Char('Code', required=True)

ContractPartyType()



class ContractParty(ModelSQL, ModelView):
    _name = 'contract.party'

    type = fields.Many2One('contract.party.type', 'Type',
            states = {
                'required':
                    Bool(Get(Eval('_parent_contract', {}), 'automation'))
                })

ContractParty()
