# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Contract Automation',
    'name_de_DE': 'Vertrag Automatik',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Contract Automation
    - Provides the base functionalities for time based handling of contract
      handling
''',
    'description_de_DE': '''Vertrag Automatik
    - Stellt die Funktionen für die zeitgesteuerte Abwicklung von Verträgen
      bereit
''',
    'depends': [
        'contract',
    ],
    'xml': [
        'contract.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
